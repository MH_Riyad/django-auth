from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LoginView,LogoutView

from . import views

app_name = 'accounts'
urlpatterns = [
    path('', views.Dashboard.as_view(), name = 'dashboard'),
    path('login/',LoginView.as_view(), name ='login'),
    path('logout/',LogoutView.as_view(), name = 'logout')
]   
